package advprog.example.bot.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("An EchoHandler")
class EchoHandlerTest {

    @Test
    @DisplayName("echoes back the same message correctly")
    void testEcho() {
        assertEquals("Lorem Ipsum", EchoHandler.echo("Lorem Ipsum"));
    }

    @Test
    @DisplayName("echoes empty message")
    void testEchoWhenEmpty() {
        assertEquals("", EchoHandler.echo(""));
    }
}
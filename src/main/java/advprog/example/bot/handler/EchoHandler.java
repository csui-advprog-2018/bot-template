package advprog.example.bot.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EchoHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(EchoHandler.class);

    public static String echo(String input) {
        LOGGER.debug(String.format("input: '%s'", input));

        return input;
    }
}

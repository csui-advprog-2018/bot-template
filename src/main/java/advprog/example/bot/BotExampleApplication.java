package advprog.example.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotExampleApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(BotExampleApplication.class);

    public static void main(String[] args) {
        LOGGER.debug("Application starting ...");
        SpringApplication.run(BotExampleApplication.class, args);
    }
}

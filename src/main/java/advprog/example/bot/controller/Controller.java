package advprog.example.bot.controller;

import advprog.example.bot.handler.EchoHandler;
import advprog.example.bot.util.TextUtil;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@LineMessageHandler
public class Controller {

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    @EventMapping
    public TextMessage handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        LOGGER.debug(String.format("TextMessageContent(timestamp='%s',content='%s')",
            event.getTimestamp(), event.getMessage()));
        TextMessageContent content = event.getMessage();
        String commandText = content.getText();

        if (commandText.startsWith("/echo")) {
            String args = TextUtil.getCommandArguments(commandText);
            String reply = EchoHandler.echo(args);

            return new TextMessage(reply);
        }

        return new TextMessage("");
    }

    @EventMapping
    public void handleDefaultMessage(Event event) {
        LOGGER.debug(String.format("Event(timestamp='%s',source='%s')",
            event.getTimestamp(), event.getSource()));
    }
}
